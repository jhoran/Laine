/* exported MPRISControl */

const {Clutter, GLib, Gio, St, Shell} = imports.gi;

const Main = imports.ui.main;
const Slider = imports.ui.slider;
const Loop = imports.mainloop;
const BoxPointer = imports.ui.boxpointer;


const WindowTracker = Shell.WindowTracker.get_default();
const Me = imports.misc.extensionUtils.getCurrentExtension();
const Gettext = imports.gettext.domain(Me.metadata['gettext-domain']);
const _ = Gettext.gettext;

const {StreamBase} = Me.imports.simpleStream;

const WATCH_RULE = "type='signal'," +
        "sender='org.freedesktop.DBus'," +
        "interface='org.freedesktop.DBus'," +
        "member='NameOwnerChanged'," +
        "path='/org/freedesktop/DBus'," +
        "arg0namespace='org.mpris.MediaPlayer2'";

Gio._promisify(Gio, 'bus_get', 'bus_get_finish');

var MPRISControl = class {
    constructor(parent, paconn) {
        this._parent = parent;
        this._paDBus = paconn;
        this.actor = parent.actor;

        this._mprisStreams = {};

        Gio.bus_get(Gio.BusType.SESSION, null)
            .then(this._hdlBusConnection.bind(this))
            .catch(logError);

        this.actor.connect('destroy', this._onDestroy.bind(this));
    }

    async _hdlBusConnection(dbus) {
        this._dbus = dbus;

        let names = await dbus.call(
            'org.freedesktop.DBus', '/', 'org.freedesktop.DBus', 'ListNames', null,
            GLib.VariantType.new('(as)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0));

        const addP = [];
        for (let i = 0; i < names.n_children(); i++) {
            let path = names.get_child_value(i).get_string()[0];
            if (path.search('^org.mpris.MediaPlayer2') == 0)
                addP.push(this._addMPRISStream(path, null));
        }

        await Promise.all(addP);
        this._parent._addExistingStreams();

        await this._dbus.call(
            'org.freedesktop.DBus', '/', 'org.freedesktop.DBus', 'AddMatch',
            GLib.Variant.new('(s)', [WATCH_RULE]), null, Gio.DBusCallFlags.NONE, -1, null);
        this._sigNOC = this._dbus.signal_subscribe(
            'org.freedesktop.DBus', 'org.freedesktop.DBus', 'NameOwnerChanged',
            '/org/freedesktop/DBus', null, Gio.DBusSignalFlags.NO_MATCH_RULE,
            this._onConnChange.bind(this));
    }

    async _addMPRISStream(path, uname) {
        let pid = await this._dbus.call(
            'org.freedesktop.DBus', '/', 'org.freedesktop.DBus', 'GetConnectionUnixProcessID',
            GLib.Variant.new('(s)', [path]), GLib.VariantType.new('(u)'),
            Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).get_uint32());
        if (pid in this._mprisStreams)
            return;

        if (uname == null)
            uname = await this._dbus.call(
                'org.freedesktop.DBus', '/', 'org.freedesktop.DBus', 'GetNameOwner',
                GLib.Variant.new('(s)', [path]), GLib.VariantType.new('(s)'),
                Gio.DBusCallFlags.NONE, -1, null)
                .then(r => r.get_child_value(0).unpack());

        if (uname == null)
            return;

        let nStr = new MPRISStream(this._parent, uname, pid, this._dbus, this._paDBus);
        this._mprisStreams[pid] = nStr;
        this.actor.add(nStr.actor);
    }

    removePAStream(path) {
        for (let pid in this._mprisStreams) {
            if (this._mprisStreams[pid]._paPath == path) {
                this._mprisStreams[pid].unsetPAStream();
                break;
            }
        }
    }

    isMPRISStream(pid, path) {
        if (pid in this._mprisStreams) {
            let obj = this._mprisStreams[pid];
            if (obj == '')
                //The pid has been registered however the object hasn't been created yet, so a
                //timeout of 2 seconds lapses before trying to reregister the pa stream.
                Loop.timeout_add_seconds(2, () => {
                    try {
                        this._mprisStreams[pid].setPAStream(path);
                    } catch (e) {
                        log(`Laine Exception: ${e}`);
                    }
                });
            else
                obj.setPAStream(path);
            return true;
        }
        return false;
    }

    _onConnChange(conn, sender, object, iface, signal, param) {
        let path = param.get_child_value(0).get_string()[0];
        let add = (param.get_child_value(1).get_string()[0] == '');

        if (path.search('^org.mpris.MediaPlayer2') != 0)
            return;

        if (add) {
            let uName = param.get_child_value(2).get_string()[0];
            this._addMPRISStream(path, uName).catch(logError);
        } else {
            for (let k in this._mprisStreams) {
                let uName = param.get_child_value(1).get_string()[0];
                if (this._mprisStreams[k]._path == uName) {
                    this._mprisStreams[k].destroy();
                    delete this._mprisStreams[k];
                    this.actor.queue_relayout();
                    break;
                }
            }
        }
    }

    _onDestroy() {
        this._dbus.signal_unsubscribe(this._sigNOC);
        this._dbus.call('org.freedesktop.DBus', '/', 'org.freedesktop.DBus', 'RemoveMatch',
            GLib.Variant.new('(s)', [WATCH_RULE]), null, Gio.DBusCallFlags.NONE, -1, null)
            .catch(logError);
    }

};


var MPRISStream = class extends StreamBase {
    // Name: 'MPRISStream',
    // Extends: StreamBase,

    constructor(parent, dbusPath, pid, dbus, paconn) {
        super(parent, paconn);
        this._path = dbusPath;
        this._procID = pid;
        this._dbus = dbus;
        this._mediaLength = 0;
        this._sigFVol = this._sigFMute = -1;
        this.actor.add_style_class_name('mpris-stream');
        this._label.add_style_pseudo_class('clickable');

        this.unsetPAStream();

        this._songLbl = new St.Label({style_class: 'mpris-meta-title'});
        this._artistLbl = new St.Label({style_class: 'mpris-meta-info'});
        this._albumLbl = new St.Label({style_class: 'mpris-meta-info'});
        this._albumArt = new St.Icon({style_class: 'mpris-album-art'});

        this._playBtn = new St.Button({style_class: 'mpris-play-button',
            child: new St.Icon({icon_name: 'media-playback-start-symbolic'}),
            reactive: true,
            can_focus: true});
        this._prevBtn = new St.Button({style_class: 'mpris-previous-button',
            child: new St.Icon({icon_name: 'media-skip-backward-symbolic'}),
            reactive: true,
            can_focus: true});
        this._nextBtn = new St.Button({style_class: 'mpris-next-button',
            child: new St.Icon({icon_name: 'media-skip-forward-symbolic'}),
            reactive: true,
            can_focus: true});

        this._posSlider = new Slider.Slider(0);
        this._timeLapLbl = new St.Label({style_class: 'mpris-time-label'});
        this._timeRemLbl = new St.Label({style_class: 'mpris-time-label'});

        this._artistBox = new St.BoxLayout();
        this._artistBox.add(new St.Label({text: _('by'), style_class: 'mpris-label-subtext'}));
        this._artistBox.add(this._artistLbl);
        this._albumBox = new St.BoxLayout();
        this._albumBox.add(new St.Label({text: _('from'), style_class: 'mpris-label-subtext'}));
        this._albumBox.add(this._albumLbl);
        this._detailBox = new St.BoxLayout({vertical: true});
        this._detailBox.add(this._songLbl);
        this._detailBox.add(this._artistBox);
        this._detailBox.add(this._albumBox);
        this._sigUpdPos = 0;

        let mediaControls = new St.BoxLayout({style_class: 'mpris-player-controls'});
        mediaControls.add(this._prevBtn);
        mediaControls.add(this._playBtn);
        mediaControls.add(this._nextBtn);

        let innerBox = new St.BoxLayout({vertical: true, style_class: 'mpris-info-and-controls'});
        innerBox.add(this._detailBox);
        innerBox.add(mediaControls);

        this._metaDisplay = new St.BoxLayout({style_class: 'mpris-metadata-display'});
        this._metaDisplay.add(this._albumArt);
        this._metaDisplay.add(innerBox);

        this._timeBox = new St.BoxLayout({style_class: 'mpris-time-display'});
        this._timeBox.add(this._timeLapLbl);
        this._timeBox.add(this._posSlider);//, {expand: true});
        this._timeBox.add(this._timeRemLbl);

        this.actor.add(this._metaDisplay);
        this.actor.add(this._timeBox);//, {expand: true});

        this._setProperties().catch(logError);

        //Signal handlers
        this._sigPropChange = this._dbus.signal_subscribe(
            this._path, 'org.freedesktop.DBus.Properties', 'PropertiesChanged',
            '/org/mpris/MediaPlayer2', null, Gio.DBusSignalFlags.NONE,
            this._onPropChange.bind(this));
        this._sigSeeked = this._dbus.signal_subscribe(
            this._path, 'org.mpris.MediaPlayer2.Player', 'Seeked', '/org/mpris/MediaPlayer2', null,
            Gio.DBusSignalFlags.NONE, this._onPropChange.bind(this));

        this._posSlider.connect('notify::value', (s) => this._onPosSliderChange(s).catch(logError));

        this._playBtn.connect('clicked', this._onControlBtnClick.bind(this));
        this._nextBtn.connect('clicked', this._onControlBtnClick.bind(this));
        this._prevBtn.connect('clicked', this._onControlBtnClick.bind(this));

        this.actor.connect('destroy', this._onDestroy.bind(this));

        this._label.connect('button-press-event', () => this._raise().catch(logError));

        this._volSlider.set_track_hover(true);
        this._volSlider.connect('key-press-event', this._onVolSliderKeyPress.bind(this));

        this._volSlider.connect('notify::hover',
            () => this._setFocused(this._volSlider.hover, 'active-top'));
        this._posSlider.set_track_hover(true);
        this._posSlider.connect('notify::hover',
            () => this._setFocused(this._posSlider.hover, 'active-bottom'));
        this._label.connect('notify::hover',
            () => this._setFocused(this._label.hover, 'active-top'));


        this._muteBtn.set_track_hover(true);
        this._muteBtn.connect('notify::hover',
            () => this._setFocused(this._muteBtn.hover, 'active-top'));

        this._volSlider.connect('key-focus-in',
            () => this._setFocused(true, 'active-top'));
        this._volSlider.connect('key-focus-out',
            () => this._setFocused(false, 'active-top'));
        this._posSlider.connect('key-focus-in',
            () => this._setFocused(true, 'active-bottom'));
        this._posSlider.connect('key-focus-out',
            () => this._setFocused(false, 'active-bottom'));
    }

    async _setProperties() {
        const dName = await this._dbus.call(
            this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2', 'DesktopEntry']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack().get_string()[0]);

        let icon;
        let app = Shell.AppSystem.get_default().lookup_app(`${dName}.desktop`);
        if (app != null) {
            let info = app.get_app_info();
            this._label.text = info.get_name();
            icon = new St.Icon({style_class: 'icon'});
            icon.set_gicon(info.get_icon());
        } else {
            icon = new St.Icon({icon_name: 'applications-multimedia-symbolic',
                style_class: 'simple-stream-icon'});
            this._label.text = await this._dbus.call(
                this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Get',
                GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2', 'Identity']),
                GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
                .then(r => r.get_child_value(0).unpack().get_string()[0]);
        }

        this._muteBtn.child = icon;

        const metadata = await this._dbus.call(
            this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2.Player', 'Metadata']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack());
        this._updateMetadata(metadata);

        const playbackStatus = await this._dbus.call(
            this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2.Player', 'PlaybackStatus']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack().get_string()[0]);

        if (playbackStatus == 'Playing')
            await this._setStatePlaying();
        else if (playbackStatus == 'Paused') {
            this._mediaPosition = await this._dbus.call(
                this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Get',
                GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2.Player', 'Position']),
                GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
                .then(r => r.get_child_value(0).unpack().get_int64());
            this._timeLapLbl.text = this._formatSeconds(Math.floor(this._mediaPosition / 1000000));
            let rem = Math.floor((this._mediaLength - this._mediaPosition) / 1000000);
            this._timeRemLbl.text = `-${this._formatSeconds(rem)}`;
            this._posSlider.value = this._mediaPosition / this._mediaLength;

        } else if (playbackStatus == 'Stopped') {
            this._prevBtn.hide();
            this._nextBtn.hide();
            this._timeBox.hide();
            this._detailBox.hide();
            this._albumArt.hide();

            this.actor.set_vertical(false);
            this.actor.add_style_pseudo_class('alone');
            this._metaDisplay.add_style_pseudo_class('alone');
            this._playBtn.add_style_pseudo_class('alone');
            this._volCtrl.add_style_pseudo_class('alone');
        }
    }


    _setFocused(activate, type) {
        if (activate)
            this.actor.add_style_pseudo_class(type);
        else
            this.actor.remove_style_pseudo_class(type);
    }

    setPAStream(path) {
        if (this._sigFVol != -1) {
            this._volSlider.disconnect(this._sigFVol);
            this._muteBtn.disconnect(this._sigFMute);

            this._sigFMute = this._sigFVol = -1;
        }

        this.setPAPath(path);
    }

    unsetPAStream() {
        if (this._paPath) {
            this._paDBus.signal_unsubscribe(this._sigVol);
            this._paDBus.signal_unsubscribe(this._sigMute);
        }

        this._paPath = null;

        this._sigFVol = this._volSlider.connect('notify::value', (slider) => {
            this._dbus.call(
                this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Set',
                GLib.Variant.new('(ssv)', ['org.mpris.MediaPlayer2.Player', 'Volume',
                    GLib.Variant.new_double(slider.value)]),
                null, Gio.DBusCallFlags.NONE, -1, null)
                .catch(logError);
        });
        this._sigFMute = this._muteBtn.connect('clicked', () => {
            this._muteVal = !this._muteVal;
            this._dbus.call(
                this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Set',
                GLib.Variant.new('(ssv)', ['org.mpris.MediaPlayer2.Player', 'Volume',
                    GLib.Variant.new_double(this._muteVal ? 0 : this._appVol)]),
                null, Gio.DBusCallFlags.NONE, -1, null)
                .catch(logError);
        });
    }


    _updateMetadata(meta) {
        if (meta == null || meta.n_children() == 0) {
            this._prevBtn.hide();
            this._nextBtn.hide();
            this._timeBox.hide();
            this._detailBox.hide();
            this._albumArt.hide();

            this.actor.set_vertical(false);
            this.actor.add_style_pseudo_class('alone');
            this._metaDisplay.add_style_pseudo_class('alone');
            this._playBtn.add_style_pseudo_class('alone');
            this._volCtrl.add_style_pseudo_class('alone');

            return;
        }

        this.actor.set_vertical(true);
        this.actor.remove_style_pseudo_class('alone');
        this._metaDisplay.remove_style_pseudo_class('alone');
        this._playBtn.remove_style_pseudo_class('alone');
        this._volCtrl.remove_style_pseudo_class('alone');

        this._prevBtn.show();
        this._nextBtn.show();

        let metaD = {};
        for (let i = 0; i < meta.n_children(); i++) {
            let [key, val] = meta.get_child_value(i).unpack();

            key = key.get_string()[0];
            val = val.unpack();
            metaD[key] = val;
        }

        if ('xesam:title' in metaD) {
            this._songLbl.text = metaD['xesam:title'].get_string()[0];
            this._songLbl.show();
            this._detailBox.show();
        } else {
            this._songLbl.hide();
        }

        if ('xesam:artist' in metaD) {
            let artists = metaD['xesam:artist'];
            let str = artists.get_child_value(0).get_string()[0];

            for (let i = 1; i < artists.n_children(); i++)
                str += `, ${artists.get_child_value(i).get_string()[0]}`;

            this._artistLbl.text = str;
            this._artistBox.show();
        } else {
            this._artistBox.hide();
        }

        if ('xesam:album' in metaD) {
            this._albumLbl.text = metaD['xesam:album'].get_string()[0];
            this._albumBox.show();
        } else {
            this._albumBox.hide();
        }
        try {
            if ('mpris:artUrl' in metaD) {
                let filePath = metaD['mpris:artUrl'].get_string()[0];
                filePath = decodeURI(filePath);

                if (filePath.match(/http/)) {
                    let file = Gio.file_new_for_uri(filePath);
                    file.read_async(null, null, this._setIcon.bind(this));
                } else if (filePath.match(/file/)) {
                    let iconPath = filePath.substring(7, filePath.length);
                    if (GLib.file_test(iconPath, GLib.FileTest.EXISTS)) {
                        let file = Gio.File.new_for_path(iconPath);
                        this._setIcon(file, null);
                    }
                } else
                    throw 'album art url misformed';
            } else
                throw 'no album art';

            this._albumArt.remove_style_pseudo_class('generic');
        } catch (err) {
            this._albumArt.icon_name = 'folder-music-symbolic';
            this._albumArt.add_style_pseudo_class('generic');
        } finally {
            this._albumArt.show();
        }

        if ('mpris:trackid' in metaD || 'xesam:url' in metaD) {
            if ('mpris:trackid' in metaD)
                this._mediaID = metaD['mpris:trackid'].get_string()[0];

            this._dbus.call(
                this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Get',
                GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2.Player', 'Position']),
                GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
                .then(r => this._mediaPosition = r.get_child_value(0).unpack().get_int64())
                .catch(logError);
        }

        this._mediaLength = 0;
        if ('mpris:length' in metaD) {
            this._mediaLength = metaD['mpris:length'].get_int64();

            if (this._mediaLength == -1 && this._label.text == 'VLC media player') {
                //VLC sends this field as a negative number when you are listening for the
                //value, but sends the correct reply if you ask for metadata explicatly.
                //So lets try once asking directly for metadata.
                this._dbus.call(
                    this._path, '/org/mpris/MediaPlayer2',
                    'org.freedesktop.DBus.Properties', 'Get',
                    GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2.Player', 'Metadata']),
                    GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
                    .then(r => {
                        let meta = r.get_child_value(0).unpack();
                        for (let i = 0; i < meta.n_children(); i++) {
                            let [key, val] = meta.get_child_value(i).unpack();
                            key = key.get_string()[0];
                            if (key == 'mpris:length') {
                                this._mediaLength = val.unpack().get_int64();
                                break;
                            }
                        }
                    })
                    .catch(logError);
            }
        }

        if (this._mediaLength <= 0)
            this._timeBox.hide();
        else
            this._timeBox.show();
    }

    async _setStatePlaying() {
        this._playBtn.child.icon_name = 'media-playback-pause-symbolic';

        const positionP = this._dbus.call(
            this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2.Player', 'Position']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack());
        const rateP = this._dbus.call(
            this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2.Player', 'Rate']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack());

        try {
            this._mediaPosition = (await positionP).get_int64();
            if (this._sigUpdPos == 0)
                this._sigUpdPos = Loop.timeout_add_seconds(1, this._updatePosition.bind(this));
            this._mediaRate = (await rateP).get_double();
        } catch (err) {
            //not all players support position so if this exception is thrown, do nothing.
            const skip = 'GDBus.Error:org.freedesktop.DBus.Error.InvalidArgs: No such property';
            if (!err.message.startsWith(skip))
                throw err;
        }
    }

    _onPropChange(conn, sender, object, iface, signal, param) {
        if (signal == 'PropertiesChanged') {
            let sIface = param.get_child_value(0).get_string()[0];

            if (sIface == 'org.mpris.MediaPlayer2.Player') {
                let sigs = param.get_child_value(1);
                for (let i = 0; i < sigs.n_children(); i++) {
                    let [key, val] = sigs.get_child_value(i).unpack();
                    key = key.get_string()[0];
                    val = val.unpack();

                    if (key == 'Metadata')
                        this._updateMetadata(val);
                    else if (key == 'PlaybackStatus') {
                        let state = val.get_string()[0];
                        if (state == 'Playing') {
                            this._setStatePlaying().catch(logError);
                        } else {
                            if (this._sigUpdPos != 0) {
                                Loop.source_remove(this._sigUpdPos);
                                this._sigUpdPos = 0;
                            }
                            this._playBtn.child.icon_name = 'media-playback-start-symbolic';
                        }
                    } else if (key == 'Volume' && this._paPath != null) {
                        let vol = val.get_double();
                        if (!this._muteVal)
                            this._appVol = vol;
                        if (this._paPath == null)
                            this._volSlider.value = vol;
                    }
                }
            }
        } else if (signal == 'Seeked') {
            //Have to manually get the time as banshee doesn't send it.
            this._dbus.call(
                this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Get',
                GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2.Player', 'Position']),
                GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
                .then(r => this._mediaPosition = r.get_child_value(0).unpack().get_int64())
                .catch(logError);
        }
    }

    _onControlBtnClick(button) {
        let command;
        if (button == this._playBtn)
            command = 'PlayPause';
        else if (button == this._prevBtn)
            command = 'Previous';
        else if (button == this._nextBtn)
            command = 'Next';
        else
            return;

        this._dbus.call(
            this._path, '/org/mpris/MediaPlayer2', 'org.mpris.MediaPlayer2.Player', command,
            null, null, Gio.DBusCallFlags.NONE, -1, null)
            .catch(logError);
    }

    _onVolSliderKeyPress(actor, event) {
        let key = event.get_key_symbol();

        if (key == Clutter.KEY_space || key == Clutter.KEY_Return) {
            this.setVolume(!this._muteVal);
            return Clutter.EVENT_STOP;
        }
        return Clutter.EVENT_PROPAGATE;
    }

    async _onPosSliderChange(slider) {
        if (this._mediaLength != 0) {
            let position = Math.floor(slider.value * this._mediaLength);
            await this._dbus.call(
                this._path, '/org/mpris/MediaPlayer2', 'org.mpris.MediaPlayer2.Player',
                'SetPosition', GLib.Variant.new('(ox)', [this._mediaID, position]), null,
                Gio.DBusCallFlags.NONE, -1, null);
        }
    }

    _updatePosition() {
        if (this._mediaLength <= 0 || this._mediaLength < this._mediaPosition)
            return;

        this._sigUpdPos = Loop.timeout_add_seconds(1, this._updatePosition.bind(this));

        this._mediaPosition += 1000000 * this._mediaRate;
        this._timeLapLbl.text = this._formatSeconds(Math.floor(this._mediaPosition / 1000000));
        let rem = Math.floor((this._mediaLength - this._mediaPosition) / 1000000);
        this._timeRemLbl.text = `-${this._formatSeconds(rem)}`;
        this._posSlider._value = this._mediaPosition / this._mediaLength;
    }

    _formatSeconds(seconds) {
        let mod = seconds % 60;
        let ans = mod.toString();
        if (mod < 10)
            ans = `0${ans}`;
        seconds = Math.floor(seconds / 60);
        if (seconds > 0) {
            ans = `${seconds % 60  }:${  ans}`;
            seconds = Math.floor(seconds / 60);
        } else
            ans = `0:${ans}`;
        if (seconds > 0)
            ans = `${seconds }:${ans}`;
        return ans;
    }

    async _raise() {
        if (this._app == null)
            this._app = WindowTracker.get_app_from_pid(this._procID);

        if (this._app == null) {//Check the tray
            let trayNotifications = Main.messageTray.getSources();
            for (let i = 0; i < trayNotifications.length; i++)
                if (trayNotifications[i].pid == this._procID)
                    this._app = trayNotifications[i].app;
        }

        if (this._app == null) {//try raising a window via dbus
            const canRaise = await this._dbus.call(
                this._path, '/org/mpris/MediaPlayer2', 'org.freedesktop.DBus.Properties', 'Get',
                GLib.Variant.new('(ss)', ['org.mpris.MediaPlayer2', 'CanRaise']),
                GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
                .then(r => r.get_child_value(0).unpack().get_boolean());

            if (canRaise) {
                await this._dbus.call(
                    this._path, '/org/mpris/MediaPlayer2', 'org.mpris.MediaPlayer2', 'Raise',
                    null, null, Gio.DBusCallFlags.NONE, -1, null);

                this._app = WindowTracker.get_app_from_pid(this._procID);
            }
        } else {
            this._app.activate();
            this._parent._getTopMenu().close(BoxPointer.PopupAnimation.FULL);
        }
    }

    _setIcon(file, result) {
        if (result != null) {
            if (this._tmpIcon)
                this._tmpIcon.delete(null);
            this._tmpIcon = Gio.file_new_tmp('laine.XXXXXX')[0];

            let inStr = file.read_finish(result);
            let outStr = this._tmpIcon.replace(null, false,
                Gio.FileCreateFlags.REPLACE_DESTINATION, null, null);
            outStr.splice(inStr,
                Gio.OutputStreamSpliceFlags.CLOSE_SOURCE |
                Gio.OutputStreamSpliceFlags.CLOSE_TARGET, null);
            file = this._tmpIcon;
        }

        let icon = new Gio.FileIcon({file: file});
        this._albumArt.set_gicon(icon);

        this._albumArt.show();
    }

    _onDestroy() {
        this._dbus.signal_unsubscribe(this._sigPropChange);
        this._dbus.signal_unsubscribe(this._sigSeeked);
        if (this._paPath) {
            this._paDBus.signal_unsubscribe(this._sigVol);
            this._paDBus.signal_unsubscribe(this._sigMute);
        }
        if (this._sigUpdPos != 0) {
            Loop.source_remove(this._sigUpdPos);
            this._sigUpdPos = 0;
        }

        if (this._tmpIcon)
            this._tmpIcon.delete(null);
    }
};
