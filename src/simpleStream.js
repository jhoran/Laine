/* exported SteamBase, SimpleStream */

const {GLib, Gio, St, Shell, Clutter} = imports.gi;

const PopupMenu = imports.ui.popupMenu;
const Main = imports.ui.main;
const Slider = imports.ui.slider;
const BoxPointer = imports.ui.boxpointer;

const WindowTracker = Shell.WindowTracker.get_default();

const PA_MAX = 65536;

var StreamBase = class extends PopupMenu.PopupMenuSection {
    constructor(parent, paconn) {
        super();
        this._paDBus = paconn;
        this._paPath = null;
        this._parent = parent;

        this._label = new St.Label({style_class: 'simple-stream-label',
            reactive: true, track_hover: true});
        this._muteBtn = new St.Button();
        this._volSlider = new Slider.Slider(0);
        //------------------------------------------------------------------
        //Laying out components
        let container = new St.BoxLayout({vertical: true, x_expand: true});
        container.add(this._label);
        container.add(this._volSlider);

        this._volCtrl = new St.BoxLayout({x_expand: true});
        this._volCtrl.add(this._muteBtn);
        this._volCtrl.add(container);

        this._volCtrl.add_style_class_name('stream');

        this.actor.set_vertical(false);
        this.actor.set_track_hover(true);
        this.actor.set_reactive(true);
        this.focused = false;

        this.actor.add(this._volCtrl);

        //------------------------------------------------------------------

        this._muteBtn.connect('clicked', () => this.setVolume(!this._muteVal));

        this._volSlider.connect('notify::value', (slider) => this.setVolume(slider.value));


        this.actor.connect('destroy', this._onDestroy.bind(this));
    }

    async setPAPath(path) {
        this._paPath = path;

        const muteP = this._paDBus.call(
            null, this._paPath, 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.PulseAudio.Core1.Stream', 'Mute']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack())
            .then(this.setVolume.bind(this));

        const volP = this._paDBus.call(
            null, this._paPath, 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.PulseAudio.Core1.Stream', 'Volume']),
            GLib.VariantType.new('(v)'),
            Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack())
            .then(this.setVolume.bind(this));

        await Promise.all([muteP, volP]);

        this._sigVol = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1.Stream', 'VolumeUpdated', this._paPath, null,
            Gio.DBusSignalFlags.NONE,
            (c, s, o, i, si, param) => this.setVolume(param.get_child_value(0)));

        this._sigMute = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1.Stream', 'MuteUpdated', this._paPath, null,
            Gio.DBusSignalFlags.NONE,
            (c, s, o, i, si, param) => this.setVolume(param.get_child_value(0)));
    }

    setVolume(volume) {
        if (typeof volume === 'boolean' && this._paPath != null) {
            let val = GLib.Variant.new_boolean(volume);
            this._paDBus.call(
                null, this._paPath, 'org.freedesktop.DBus.Properties', 'Set',
                GLib.Variant.new('(ssv)', ['org.PulseAudio.Core1.Stream', 'Mute', val]), null,
                Gio.DBusCallFlags.NONE, -1, null)
                .catch(logError);
        } else if (typeof volume === 'number' && this._paPath != null) {
            if (volume > 1)
                volume = 1;
            let max = this._volVariant.get_child_value(0).get_uint32();
            for (let i = 1; i < this._volVariant.n_children(); i++) {
                let val = this._volVariant.get_child_value(i).get_uint32();
                if (val > max)
                    max = val;
            }

            let target = volume * PA_MAX;
            if (target != max) { //Otherwise no change
                let targets = new Array();
                for (let i = 0; i < this._volVariant.n_children(); i++) {
                    let newVal;
                    if (max == 0)
                        newVal = target;
                    else { //To maintain any balance the user has set.
                        let oldVal = this._volVariant.get_child_value(i).get_uint32();
                        newVal = (oldVal / max) * target;
                    }
                    newVal = Math.round(newVal);
                    targets[i] = GLib.Variant.new_uint32(newVal);
                }
                targets = GLib.Variant.new_array(null, targets);
                this._paDBus.call(null, this._paPath, 'org.freedesktop.DBus.Properties', 'Set',
                    GLib.Variant.new('(ssv)', ['org.PulseAudio.Core1.Stream', 'Volume', targets]),
                    null, Gio.DBusCallFlags.NONE, -1, null)
                    .catch(logError);
                if (this._muteVal)
                    this.setVolume(false);
            }
        } else if (volume instanceof GLib.Variant) {
            let type = volume.get_type_string();
            if (type == 'au') {
                this._volVariant = volume;
                if (!this._muteVal) {
                    let maxVal = volume.get_child_value(0).get_uint32();
                    for (let i = 1; i < volume.n_children(); i++) {
                        let val = volume.get_child_value(i).get_uint32();
                        if (val > maxVal)
                            maxVal = val;
                    }

                    this._volSlider.value = maxVal / PA_MAX;
                }
            } else if (type == 'b') {
                this._muteVal = volume.get_boolean();
                if (this._muteVal)
                    this._volSlider.value = 0;
                else if (this._volVariant)
                    this.setVolume(this._volVariant);
            }
        }
    }

    _setFocused(activate) {
        let focusChanged = activate != this.focused;
        if (focusChanged) {
            this.focused = activate;
            if (activate) {
                this.actor.add_style_pseudo_class('active');
                this.actor.grab_key_focus();
            } else {
                this.actor.remove_style_pseudo_class('active');
            }

        }
    }

    _onDestroy() {
        if (this._paPath != null) {
            this._paDBus.signal_unsubscribe(this._sigVol);
            this._paDBus.signal_unsubscribe(this._sigMute);
        }
    }

    _raise() {}
};


var SimpleStream = class extends StreamBase {
    // Name: 'SimpleStream',
    // Extends: StreamBase,

    constructor(parent, paconn, path, sInfo) {
        super(parent, paconn);
        this.setPAPath(path).catch(logError);

        this.actor.add_style_class_name('simple-stream');

        this._procID = parseInt(sInfo['application.process.id']);

        this._app = WindowTracker.get_app_from_pid(this._procID);
        if (this._app == null) {
            //Doesn't have an open window, lets check the tray.
            let trayNotifications = Main.messageTray.getSources();
            for (let i = 0; i < trayNotifications.length; i++)
                if (trayNotifications[i].pid == this._procID)
                    this._app = trayNotifications[i].app;
        }

        let icon, name = null;
        if (this._app != null) {
            let info = this._app.get_app_info();
            if (info != null) {
                name = info.get_name();
                let _icon = info.get_icon();
                if (_icon != null)
                    icon = new St.Icon({
                        gicon: _icon,
                        fallback_icon_name: 'applications-multimedia-symbolic',
                        style_class: 'icon'
                    });
            }

            if (name == null)
                name = this._app.get_name();

            if (icon == null) {
                let icon_texture = this._app.create_icon_texture(32);
                if (icon_texture instanceof Clutter.Texture) {
                    icon = new St.Bin({
                        child: icon_texture,
                        style_class: 'icon'
                    });
                }
            }
            this._label.add_style_pseudo_class('clickable');
        }

        if (icon == null) {
            icon = new St.Icon({
                style_class: 'icon',
                fallback_icon_name: 'applications-multimedia-symbolic',
            });

            if ('application.icon_name' in sInfo)
                icon.icon_name = sInfo['application.icon_name'];
        }

        if (name == null)
            name = sInfo['application.name'];

        this._muteBtn.child = icon;
        this._label.text = name;

        this._label.connect('button-press-event', () => {
            if (this._app != null) {
                this._app.activate();
                this._parent._getTopMenu().close(BoxPointer.PopupAnimation.FULL);
            }
        });

        this.actor.connect('key-press-event', this._onKeyPressEvent.bind(this));

        this.actor.can_focus = true;
        this.actor.connect('notify::hover', () => this._setFocused(this.actor.hover));
        this.actor.connect('key-focus-in', () => this._setFocused(true));
        this.actor.connect('key-focus-out', () => this._setFocused(false));
        // this.actor.connect('scroll-event', this._volSlider._onScrollEvent.bind(this._volSlider));
        // Lang.bind(this._volSlider, this._volSlider._onScrollEvent));
    }

    _onKeyPressEvent(actor, event) {
        let key = event.get_key_symbol();

        if (key == Clutter.KEY_Right || key == Clutter.KEY_Left) {
            this._volSlider.onKeyPressEvent(actor, event);
            return Clutter.EVENT_STOP;
        } else if (key == Clutter.KEY_space || key == Clutter.KEY_Return) {
            this.setVolume(!this._muteVal);
            return Clutter.EVENT_STOP;
        }

        return Clutter.EVENT_PROPAGATE;
    }
};
