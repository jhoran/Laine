const {St, GLib, Gio, Shell} = imports.gi;
const Me = imports.misc.extensionUtils.getCurrentExtension();

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Gettext = imports.gettext.domain(Me.metadata['gettext-domain']);
const _ = Gettext.gettext;

const StreamMenu = Me.imports.streamMenu;
const SinkMenu = Me.imports.sinkMenu;
const SourceMenu = Me.imports.sourceMenu;
const Convenience = Me.imports.convenience;

Gio._promisify(Gio.DBusConnection, 'new_for_address', 'new_for_address_finish');

const LaineCore = class extends PopupMenu.PopupMenuSection {
    constructor(container) {
        super();
        this._icon = new St.Icon({icon_name: 'system-run-symbolic',
            style_class: 'system-status-icon'});

        this.connectToPADBus()
          .then(this.build.bind(this, container))
          .catch(e => logError(e, 'EXCEPTION Laine'));
    }

    async build(container, conn) {
        this._paDBus = conn;

        this._sinkMenu = new SinkMenu.SinkMenu(this, this._paDBus);
        this._sourceMenu = new SourceMenu.SourceMenu(this, this._paDBus);
        this._streamMenu = new StreamMenu.StreamMenu(this, this._paDBus);

        this._sinkMenu.connect('icon-changed', this._onUpdateIcon.bind(this));
        this._sinkMenu.connect(
            'fallback-updated',
            this._streamMenu._onSetDefaultSink.bind(this._streamMenu));
        this._sourceMenu.connect(
            'fallback-updated',
            this._sourceMenu._onSetDefaultSource.bind(this._sourceMenu));

        this._setIndicatorIcon(this._sinkMenu._slider.value);
        this._addPulseAudioListeners();

        this.addMenuItem(this._sinkMenu);
        this.addMenuItem(this._sourceMenu);

        this.addMenuItem(this._streamMenu);
        this.addMenuItem(this._buildSettingsMenu());

        container.layout();
    }

    async connectToPADBus() {
        let paAddr = `unix:path=${  GLib.getenv('XDG_RUNTIME_DIR')  }/pulse/dbus-socket`;
        if (!Gio.file_new_for_path(paAddr.slice(10)).query_exists(null)) {
            paAddr = await Gio.DBus.session.call(
                'org.PulseAudio1', '/org/pulseaudio/server_lookup1',
                'org.freedesktop.DBus.Properties', 'Get',
                GLib.Variant.new('(ss)', ['org.PulseAudio.ServerLookup1', 'Address']),
                GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
          .then(r => r.get_child_value(0).unpack().get_string()[0]);
        }

        try {
            return await Gio.DBusConnection.new_for_address(
                paAddr, Gio.DBusConnectionFlags.AUTHENTICATION_CLIENT, null, null);
        } catch (e) {}


        let [, pid] = GLib.spawn_async(null,
            ['pactl', 'load-module', 'module-dbus-protocol'], null,
            GLib.SpawnFlags.SEARCH_PATH | GLib.SpawnFlags.STDOUT_TO_DEV_NULL |
            GLib.SpawnFlags.STDERR_TO_DEV_NULL | GLib.SpawnFlags.DO_NOT_REAP_CHILD,
            null);

        return new Promise((resolve, reject) => {
            const childWatch = GLib.child_watch_add(GLib.PRIORITY_DEFAULT, pid, () => {
                GLib.source_remove(childWatch);
                let paAddr = `unix:path=${GLib.getenv('XDG_RUNTIME_DIR')}/pulse/dbus-socket`;
                try {
                    let conn = Gio.DBusConnection.new_for_address(
                        paAddr, Gio.DBusConnectionFlags.AUTHENTICATION_CLIENT, null, null);
                    resolve(conn);
                } catch (e) {
                    reject(e);
                }
            });
        });


    }

    _buildSettingsMenu() {
        var m = new PopupMenu.PopupMenuItem(_('PulseAudio Settings'));
        m._key_VISIBLE = Me.imports.prefs.KEY_OPEN_SETTINGS;
        m._settings = Convenience.getSettings();

        m._setVisible = () => {
            if (m._settings.get_boolean(m._key_VISIBLE))
                m.actor.show();
            else
                m.actor.hide();
        };

        m._onClick = () => {
            var appName = m._settings.get_string('app-settings');
            var sys = Shell.AppSystem.get_default();
            let app = sys.lookup_app(`${appName}.desktop`);
            if (app != null)
                app.activate();
            else
                Main.notify(`${_('Couldn\'t load')} ${ appName }.`);
            m._getTopMenu()._container.menu.close();
        };

        m._sigShow = m._settings.connect(`changed::${m._key_VISIBLE}`, m._setVisible);
        m._sigClick = m.connect('activate', m._onClick);

        m.actor.connect('destroy', () => {
            if (m._sigShow) {
                m._settings.disconnect(m._sigShow);
                delete m._sigShow;
            }
        });

        m._setVisible();
        return m;
    }

    async _addPulseAudioListeners() {
        const signals = [
            //Stream listening
            'org.PulseAudio.Core1.NewPlaybackStream',
            'org.PulseAudio.Core1.PlaybackStreamRemoved',
            'org.PulseAudio.Core1.Stream.VolumeUpdated',
            'org.PulseAudio.Core1.Stream.MuteUpdated',

            //Sink listening
            'org.PulseAudio.Core1.Device.VolumeUpdated',
            'org.PulseAudio.Core1.Device.MuteUpdated',
            'org.PulseAudio.Core1.Device.ActivePortUpdated',
            'org.PulseAudio.Core1.NewSink',
            'org.PulseAudio.Core1.SinkRemoved',
            'org.PulseAudio.Core1.FallbackSinkUpdated',
            'org.PulseAudio.Core1.FallbackSinkUnset',

            //Source listening
            'org.PulseAudio.Core1.NewSource',
            'org.PulseAudio.Core1.SourceRemoved',
            'org.PulseAudio.Core1.FallbackSourceUpdated',
            'org.PulseAudio.Core1.FallbackSourceUnset',

            //Record listening
            'org.PulseAudio.Core1.NewRecordStream',
            'org.PulseAudio.Core1.RecordStreamRemoved'
        ];
        const promises = signals.map(s => this._paDBus.call(
            null, '/org/pulseaudio/core1', 'org.PulseAudio.Core1', 'ListenForSignal',
            GLib.Variant.new('(sao)', [s, []]),
            null, Gio.DBusCallFlags.NONE, -1, null));

        await Promise.all(promises);
    }

    _setIndicatorIcon(value) {
        if (value == 0)
            this._icon.icon_name = 'audio-volume-muted-symbolic';
        else {
            let n = Math.floor(3 * value) + 1;
            if (n < 2)
                this._icon.icon_name = 'audio-volume-low-symbolic';
            else if (n >= 3)
                this._icon.icon_name = 'audio-volume-high-symbolic';
            else
                this._icon.icon_name = 'audio-volume-medium-symbolic';
        }
    }

    _onUpdateIcon(source, value) {
        this._setIndicatorIcon(value);
    }

    _getTopMenu() {
        return this;
    }

    /**
     * This function is for tracking open child menus and closing an open one if
     * a new one opens.
     */
    _setOpenedSubMenu(menu) {
        if (this.openChildMenu != null && this.openChildMenu.isOpen) {
            this.openChildMenu.toggle();
        }

        this.openChildMenu = menu;
    }

    async _onDestroy() {
        if (!this._paDBus)
            return;
        const signals = [
            //Stream listening
            'org.PulseAudio.Core1.NewPlaybackStream',
            'org.PulseAudio.Core1.PlaybackStreamRemoved',
            'org.PulseAudio.Core1.Stream.VolumeUpdated',
            'org.PulseAudio.Core1.Stream.MuteUpdated',

            //Sink listening
            'org.PulseAudio.Core1.Device.VolumeUpdated',
            'org.PulseAudio.Core1.Device.MuteUpdated',
            'org.PulseAudio.Core1.Device.ActivePortUpdated',
            'org.PulseAudio.Core1.NewSink',
            'org.PulseAudio.Core1.SinkRemoved',
            'org.PulseAudio.Core1.FallbackSinkUpdated',
            'org.PulseAudio.Core1.FallbackSinkUnset',

            //Source listening
            'org.PulseAudio.Core1.NewSource',
            'org.PulseAudio.Core1.SourceRemoved',
            'org.PulseAudio.Core1.FallbackSourceUpdated',
            'org.PulseAudio.Core1.FallbackSourceUnset',

            //Record listening
            'org.PulseAudio.Core1.NewRecordStream',
            'org.PulseAudio.Core1.RecordStreamRemoved'
        ];
        const promises = signals.map(s => this._paDBus.call(
            null, '/org/pulseaudio/core1', 'org.PulseAudio.Core1', 'StopListeningForSignal',
            GLib.Variant.new('(sao)', [s, []]),
            null, Gio.DBusCallFlags.NONE, -1, null));

        await Promise.all(promises);


        /*  Unloading the module has caused pulseaudio to lose its connection to the sound card
            more than once, so for the sake of it it's probably a better idea to leave it loaded.

        if(this._moduleLoad){
            GLib.spawn_async(null, ['pactl','unload-module','module-dbus-protocol'], null,
                GLib.SpawnFlags.SEARCH_PATH | GLib.SpawnFlags.STDOUT_TO_DEV_NULL |
                GLib.SpawnFlags.STDERR_TO_DEV_NULL | GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                                null, null);
        }
        */
    }
};

const Laine = class {
    constructor() {
        this._settings = Convenience.getSettings();
        this._key_MERGE_CONTROLS = Me.imports.prefs.KEY_MERGE_CONTROLS;
        this._sigMerge = this._settings.connect(
            `changed::${this._key_MERGE_CONTROLS}`,
            this._switchLayout);
        this._key_ICON_POSITION = Me.imports.prefs.KEY_ICON_POSITION;
        this._sigPosChange = this._settings.connect(
            `changed::${  this._key_ICON_POSITION}`,
            this._switchEnforceIconPosition.bind(this));

        this.laineCore = new LaineCore(this);
        return 0;
    }

    layout() {
        let merge = this._settings.get_boolean(this._key_MERGE_CONTROLS);

        let stat = false;
        if (merge)
            stat = this._aggregateLayout();
        else
            stat = this._menuButtonLayout();

        if (stat) {
            Main.panel.statusArea.aggregateMenu._volume._primaryIndicator.hide();
            Main.panel.statusArea.aggregateMenu._volume._volumeMenu.actor.hide();
        }
    }

    _menuButtonLayout() {
        let hbox = new St.BoxLayout({style_class: 'panel-status-menu-box'});
        hbox.add_child(this.laineCore._icon);
        this.button = new PanelMenu.Button(0.0, '', false);
        this.button.add_child(hbox);
        this.button.menu.addMenuItem(this.laineCore);
        this.button.menu.actor.add_style_class_name('solitary');
        this.laineCore._container = this.button;

        this.button.connect('destroy', this.laineCore._onDestroy.bind(this.laineCore));

        this.button.connect(
            'scroll-event',
            this.laineCore._sinkMenu.scroll.bind(this.laineCore._sinkMenu));

        if (typeof Main.panel.statusArea.laine !== 'undefined')
            delete Main.panel.statusArea.laine;
        Main.panel.addToStatusArea('laine', this.button,
            this._settings.get_boolean(this._key_ICON_POSITION) ?
                Main.panel._rightBox.get_children().length - 1 : 0);
        this._sigPanelListen = Main.panel._rightBox.connect('actor-added', () => {
            if (!this._settings.get_boolean(this._key_ICON_POSITION))
                return;
            let container = this.button.actor.get_parent();
            let rb = Main.panel._rightBox;

            if (container == rb.get_children()[rb.get_children().length - 2])
                return;
            rb.remove_actor(container);
            rb.insert_child_at_index(container, rb.get_children().length - 1);
        }
        );

        return true;
    }

    _aggregateLayout() {
        Main.panel.statusArea.aggregateMenu.menu.addMenuItem(this.laineCore, 0);
        Main.panel.statusArea.aggregateMenu._indicators.insert_child_below(
            this.laineCore._icon,
            Main.panel.statusArea.aggregateMenu._volume._primaryIndicator.get_parent()
        );
        Main.panel.statusArea.aggregateMenu._laine = this.laineCore;
        this.laineCore._container = Main.panel.statusArea.aggregateMenu;

        this._sigScroll = Main.panel.statusArea.aggregateMenu.actor.connect(
            'scroll-event',
            this.laineCore._sinkMenu.scroll.bind(this.laineCore._sinkMenu)
        );

        return true;
    }

    _switchLayout() {
        disable();
        enable();
    }

    _switchEnforceIconPosition() {
        let container = this.button.actor.get_parent();
        Main.panel._rightBox.remove_actor(container);
        Main.panel._rightBox.insert_child_at_index(container,
            this._settings.get_boolean(this._key_ICON_POSITION) ?
                Main.panel._rightBox.get_children().length - 1 : 0);
    }

    destroy() {
        if (this.button)
            this.button.destroy();
        if (Main.panel.statusArea.aggregateMenu._laine) {
            this.laineCore._icon.destroy();
            this.laineCore.destroy();
            Main.panel.statusArea.aggregateMenu.actor.disconnect(this._sigScroll);
            delete Main.panel.statusArea.aggregateMenu._laine;
        }

        this._settings.disconnect(this._sigMerge);
        this._sigMerge = null;
        Main.panel._rightBox.disconnect(this._sigPanelListen);
        this._sigPanelListen = null;
    }
};


let _menuButton;

function init() { //eslint-disable-line no-unused-vars
    Convenience.initTranslations();
}

function enable() {
    if (typeof Main.panel.statusArea.laine === 'undefined') {
        _menuButton = null;
        _menuButton = new Laine();
    }
}

function disable() {
    _menuButton.destroy();
    Main.panel.statusArea.aggregateMenu._volume._volumeMenu.actor.show();
    Main.panel.statusArea.aggregateMenu._volume._primaryIndicator.show();
    if (Main.panel.statusArea.laine)
        delete Main.panel.statusArea.laine;
    else
        delete Main.panel.statusArea.aggregateMenu._laine;
    _menuButton = null;
}
