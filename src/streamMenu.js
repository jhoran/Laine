/* exported StreamMenu, StreamBase */
const {GLib, Gio} = imports.gi;

const PopupMenu = imports.ui.popupMenu;

const Me = imports.misc.extensionUtils.getCurrentExtension();

const {SimpleStream} = Me.imports.simpleStream;
const {MPRISControl} = Me.imports.mprisStream;


var StreamMenu = class extends PopupMenu.PopupMenuSection {
    constructor(parent, paconn) {
        super();
        this.actor.add_style_class_name('stream_container');
        this._paDBus = paconn;
        this._parent = parent;

        this._mprisControl = new MPRISControl(this, this._paDBus);
        this._streams = {};
        this._delegatedStreams = {};

        //Add any existing streams
        if (!(this._mprisControl))
            this._addExistingStreams().catch(logError);

        //Add signal handlers
        this._sigNewStr = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1', 'NewPlaybackStream', '/org/pulseaudio/core1', null,
            Gio.DBusSignalFlags.NONE, this._onAddStream.bind(this));
        this._sigRemStr = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1', 'PlaybackStreamRemoved', '/org/pulseaudio/core1', null,
            Gio.DBusSignalFlags.NONE, this._onRemoveStream.bind(this));

        this.actor.connect('destroy', this._onDestroy.bind(this));
    }

    async _addExistingStreams() {
        let streams = [];
        await this._paDBus.call(
            null, '/org/pulseaudio/core1', 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.PulseAudio.Core1', 'PlaybackStreams']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => {
                const response = r.get_child_value(0).unpack();
                for (let i = 0; i < response.n_children(); i++ )
                    streams.push(response.get_child_value(i).get_string()[0]);
            });

        if (!streams.length) {
            this.actor.add_style_pseudo_class('empty');
            return;
        }

        await Promise.all(streams.map(this._addPAStream.bind(this)));
    }

    async _addPAStream(path) {

        const streamInfo = await this._paDBus.call(
            null, path, 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.PulseAudio.Core1.Stream', 'PropertyList']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack());

        //Decode stream information
        let sInfo = {};
        for (let i = 0; i < streamInfo.n_children(); i++) {
            let [key, value] = streamInfo.get_child_value(i).unpack();
            let bytes = new Array();
            for (let j = 0; j < value.n_children(); j++)
                bytes[j] = value.get_child_value(j).get_byte();
            sInfo[key.get_string()[0]] = String.fromCharCode.apply(String, bytes);
        }

        let pID = parseInt(sInfo['application.process.id']);
        let role;
        if ('media.role' in sInfo) {
            role = sInfo['media.role'];
            role = role.substring(0, role.length - 1);
        }

        await this._moveStreamToDefaultSink(path);
        if (role == 'event')
            return;

        let mprisCheck = false;

        if (this._mprisControl) {
            mprisCheck = this._mprisControl.isMPRISStream(pID, path);
        }

        if (mprisCheck) {
            this._delegatedStreams[path] = this._mprisControl._mprisStreams[pID];
        } else {
            let stream = new SimpleStream(this, this._paDBus, path, sInfo);
            this._streams[path] = stream;
            this.addMenuItem(stream);
        }
    }

    async _moveStreamToDefaultSink(path) {
        let cPath = await this._paDBus.call(
            null, path, 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.PulseAudio.Core1.Stream', 'Device']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack().get_string()[0]);

        if (cPath != path && this._defaultSink != undefined)
            await this._paDBus.call(
                null, path, 'org.PulseAudio.Core1.Stream', 'Move',
                GLib.Variant.new('(o)', [this._defaultSink]),
                null, Gio.DBusCallFlags.NONE, -1, null);
    }

    _getTopMenu() {
        return this._parent._getTopMenu();
    }

    _onSetDefaultSink(src, sink) {
        this._defaultSink = sink;

        for (let k in this._streams)
            this._moveStreamToDefaultSink(k);

        for (let k in this._delegatedStreams) {
            let obj = this._delegatedStreams[k]._paPath;
            this._moveStreamToDefaultSink(obj);
        }
    }

    _onAddStream(conn, sender, object, iface, signal, param) {
        let streamPath = param.get_child_value(0).unpack();
        this.actor.remove_style_pseudo_class('empty');
        this._addPAStream(streamPath);
    }

    _onRemoveStream(conn, sender, object, iface, signal, param) {

        let streamPath = param.get_child_value(0).unpack();

        if (streamPath in this._streams) {
            this._streams[streamPath].destroy();
            delete this._streams[streamPath];
            this.actor.queue_relayout();
        } else if (streamPath in this._delegatedStreams) {
            this._mprisControl.removePAStream(streamPath);
            delete this._delegatedStreams[streamPath];
            this.actor.queue_relayout();
        }

        if (Object.keys(this._streams).length == 0 &&
            Object.keys(this._delegatedStreams).length == 0)
            this.actor.add_style_pseudo_class('empty');
    }

    _onDestroy() {
        this._paDBus.signal_unsubscribe(this._sigNewStr);
        this._paDBus.signal_unsubscribe(this._sigRemStr);
    }
};
