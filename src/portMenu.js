/* exported PortMenu */
const {GLib, GObject, Gio, St, Clutter} = imports.gi;

const Slider = imports.ui.slider;
const PopupMenu = imports.ui.popupMenu;

const ByteArray = imports.byteArray;
const Util = imports.misc.util;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Convenience = Me.imports.convenience;
const Tools = Me.imports.tools;
const Gettext = imports.gettext.domain(Me.metadata['gettext-domain']);
const _ = Gettext.gettext;

const PA_MAX = 65536;

const UPDATE_FALLBACK = true;
const UPDATE_FALLBACK_DELAY = 1000; //ms
const UPDATE_FALLBACK_SINK_CMD = (
    '/bin/sh -c  "/usr/bin/pacmd set-default-sink '
    + "$(/usr/bin/pacmd list-sinks | awk '/* index:/ {print $3}')\"");
const UPDATE_FALLBACK_SOURCE_CMD = (
    '/bin/sh -c  "/usr/bin/pacmd set-default-source'
    + "$(/usr/bin/pacmd list-sources | awk '/* index:/ {print $3}')\"");
const UPDATE_FALLBACK_CMD = {'Sink': UPDATE_FALLBACK_SINK_CMD,
    'Source': UPDATE_FALLBACK_SOURCE_CMD};

var PortMenu = GObject.registerClass({
    Signals: {
        'fallback-updated': {param_types: [GObject.TYPE_STRING]},
        'icon-changed': {param_types: [GObject.TYPE_DOUBLE]}
    }
},
class PortMenu extends PopupMenu.PopupSubMenuMenuItem {
    _init(parent, paconn, type) {
        super._init('', true);
        this._parent = parent;
        this._type = type;
        this._isBlockedDbus = false;

        let children = this.actor.get_children();
        this._expandBtn = children[children.length - 1];
        for (let i = 0; i < children.length - 1; i++)
            children[i].destroy();
        this.actor.remove_actor(this._expandBtn);
        this._expandBtn.hide();

        this._paDBus = paconn;
        this._devices = {};

        this._icon = new St.Icon({style_class: 'port-icon'});
        this._nameLbl = new St.Label({text: 'test'});
        let muteBtn = new St.Button({child: this._icon});
        this._slider = new Slider.Slider(0);

        //Asynchronously populate all the devices
        this._initDevices(this._paDBus, this._type).catch(logError);

        //Laying stuff out
        this.actor.add(muteBtn);
        let container = new St.BoxLayout({vertical: true, x_expand: true});
        container.add(this._nameLbl);
        container.add(this._slider);
        this.actor.add(container);
        this.actor.add(this._expandBtn);

        this.actor.add_style_class_name('port');

        //Add listeners
        this._slider.connect('notify::value', slider => this.setVolume(slider.value));

        this._slider.connect('drag-end', () => this._notifyVolumeChange());
        muteBtn.connect('clicked', () => this.setVolume(!this._activeDevice._muteVal));


        this._settings = Convenience.getSettings();
        this._key_SHOW_LABEL = Me.imports.prefs.KEY_PORT_LABEL;


        this._sigFallback = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1', `Fallback${type}Updated`, '/org/pulseaudio/core1', null,
            Gio.DBusSignalFlags.NONE, this._onDevChange.bind(this));
        this._sigFallbackUnset = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1', `Fallback${type}Unset`, '/org/pulseaudio/core1', null,
            Gio.DBusSignalFlags.NONE, this._onDevChange.bind(this));
        this._sigNewDevice = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1', `New${type}`, '/org/pulseaudio/core1', null,
            Gio.DBusSignalFlags.NONE, this._onDevChange.bind(this));
        this._sigRemovedDevice = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1', `${type}Removed`, '/org/pulseaudio/core1', null,
            Gio.DBusSignalFlags.NONE, this._onDevChange.bind(this));

        this.actor.connect('scroll-event', this.scroll.bind(this));
        this.actor.connect('destroy', this._onDestroy.bind(this));
        this._sigShowLabel = this._settings.connect(
            `changed::${this._key_SHOW_LABEL}`, this._setNameLabelVisiblity.bind(this));

        this._setNameLabelVisiblity();
    }

    async _initDevices() {
        const devices = await this._paDBus.call(
            null, '/org/pulseaudio/core1', 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.PulseAudio.Core1', `${this._type}s`]),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(Tools.unpackArrayElements);

        for (let device of devices)
            this._addDevice(device);

        await this._forceFallbackDevice();
    }

    async _forceFallbackDevice() {
        let fallbackP = this._paDBus.call(
            null, '/org/pulseaudio/core1', 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.PulseAudio.Core1', `Fallback${this._type}`]),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null);

        let fallback;
        try {
            fallback = await fallbackP.then(r => r.get_child_value(0).unpack().get_string()[0]);
        } catch (e) {
            if (!e.message.startsWith(
                'GDBus.Error:org.PulseAudio.Core1.NoSuchPropertyError:'
                    + ' There are no sinks, and therefore no fallback sink either.'))
                throw e;
            if (UPDATE_FALLBACK) {
                Util.spawnCommandLine(UPDATE_FALLBACK_CMD[this._type]);
                await new Promise((resolve) => GLib.timeout_add(
                    GLib.PRIORITY_DEFAULT, UPDATE_FALLBACK_DELAY, () => resolve()));
                fallbackP = this._paDBus.call(
                    null, '/org/pulseaudio/core1', 'org.freedesktop.DBus.Properties', 'Get',
                    GLib.Variant.new('(ss)', ['org.PulseAudio.Core1', `Fallback${this._type}`]),
                    GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null);
                fallback = await fallbackP.then(r => r.get_child_value(0).unpack().get_string()[0]);
            }
        }

        if (fallback in this._devices)
            this._setActiveDevice(this._devices[fallback]);
    }

    _onKeyPressEvent(actor, event) {
        let key = event.get_key_symbol();

        if (key == Clutter.KEY_Right || key == Clutter.KEY_Left) {
            this._slider.onKeyPressEvent(actor, event);
            return Clutter.EVENT_STOP;
        } else if (key == Clutter.KEY_space) {
            this.setVolume(!this._activeDevice._muteVal);
            return Clutter.EVENT_STOP;
        } else if (key == Clutter.KEY_Return) {
            this._setOpenState(!this._getOpenState());
            return Clutter.EVENT_STOP;
        }

        return this.parent(actor, event);
    }

    _addDevice(path) {
        let device = new Device(path, this._paDBus, this);
        this._devices[path] = device;
        // Bug in PulseAudio DBus: fallback device is not updated when it is removed
        if (UPDATE_FALLBACK == true) {
            this._forceFallbackDevice().catch(logError);
        }
    }

    _removeDevice(path) {
        if (path in this._devices) {
            // Bug in PulseAudio DBus: fallback device is not updated when it is removed
            if (UPDATE_FALLBACK == true && this._activeDevice == this._devices[path]) {
                this._forceFallbackDevice().catch(logError);
            }

            //remove device
            this._devices[path].destroy();
            delete this._devices[path];

            this._updateExpandBtnVisiblity();
        }
    }

    _unsetActiveDevice() {
        if (this._activeDevice != undefined && this._activeDevice._path in this._devices) {
            this._activeDevice.unsetActiveDevice();
        }
    }

    _setActiveDevice(dev) {
        this._unsetActiveDevice();

        this._activeDevice = dev;
        this._activeDevice.setActiveDevice().catch(logError);
    }

    setVolume(volume) {
        if (this._activeDevice != undefined) {
            this._activeDevice.setVolume(volume);
        }
    }

    _notifyVolumeChange() {
        if (this._volumeCancellable)
            this._volumeCancellable.cancel();
        this._volumeCancellable = new Gio.Cancellable();
        let player = global.display.get_sound_player();
        player.play_from_theme(
            'audio-volume-change',
            _('Volume changed'),
            this._volumeCancellable);
    }

    scroll(actor, event) {
        return this._slider.scroll(event);
    }


    //Some abstract methods
    _setMuteIcon() {}
    _isExpandBtnVisible() {}
    _isVisble() {}

    _updateExpandBtnVisiblity() {
        let set = this._isExpandBtnVisible();
        if (set)
            this._expandBtn.show();
        else
            this._expandBtn.hide();
    }

    _setNameLabelVisiblity() {
        let vis = this._settings.get_boolean(this._key_SHOW_LABEL);
        if (vis)
            this._nameLbl.show();
        else
            this._nameLbl.hide();
    }

    _updateVisibility() {
        let vis = this._isVisible();
        if (vis)
            this.actor.show();
        else
            this.actor.hide();
    }

    _onDevChange(conn, sender, object, iface, signal, param) {
        let addr = null;
        try {
            addr = param.get_child_value(0).get_string()[0];
        } catch (err) {
            if (!(err instanceof TypeError)) {
                throw err;
            }
        }

        if (signal == `Fallback${ this._type }Updated`) {
            this._setActiveDevice(this._devices[addr]);
        } else if (signal == `Fallback${ this._type }Unset`) {
            this._unsetActiveDevice();
        } else if (signal == `New${this._type}`) {
            this._addDevice(addr);
        } else if (signal == `${this._type  }Removed`) {
            this._removeDevice(addr);
        }
    }

    _onDestroy() {
        this._paDBus.signal_unsubscribe(this._sigFallback);
        this._paDBus.signal_unsubscribe(this._sigFallbackUnset);
        this._paDBus.signal_unsubscribe(this._sigNewDevice);
        this._paDBus.signal_unsubscribe(this._sigRemovedDevice);
        if (this._sigShowLabel) {
            this._settings.disconnect(this._sigShowLabel);
            delete this._sigShowLabel;
        }
    }
});

var Port = GObject.registerClass({
    Signals: {
        'name-set': {}
    }
},
class Port extends PopupMenu.PopupMenuItem {
    // Name: 'PulsePort',
    // Extends: PopupMenu.PopupMenuItem,

    _init(path, paconn, device) {
        super._init('');
        this._paDBus = paconn;
        this._path = path;
        this._device = device;
        this._type = '';

        this._paDBus.call(
            null, path, 'org.freedesktop.DBus.Properties', 'GetAll',
            GLib.Variant.new('(s)', ['org.PulseAudio.Core1.DevicePort']),
            GLib.VariantType.new('(a{sv})'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => {
                let response = r.get_child_value(0);
                for (let i = 0; i < response.n_children(); i++) {
                    let [key, value] = response.get_child_value(i).unpack();
                    key = key.get_string()[0];
                    if (key == 'Name')
                        this._type = value.unpack().get_string()[0];
                    else if (key == 'Description')
                        this._name = value.unpack().get_string()[0];
                }

                this.label.set_text(this.getName());
                this.emit('name-set');
            })
            .catch(logError);
        // catch (err) {
        ////pulseaudio does something which creates, and then deletes a card when switching sources.
        ////Therefore the listener will trigger this, but by the time we can query anything from it,
        ////it's already gone.  Therefore ignore this exception.
        //if (!err.message.startsWith('GDBus.Error:org.freedesktop.DBus.Error.UnknownMethod:
        ////Method "GetAll" with signature'))
        //throw err;
        //}
        // })

        this.connect('activate', this._device._onPortSelect.bind(this._device));
    }

    getName() {
        let name = this._device._name;
        if (this._device._numPorts > 1)
            name = name.concat(': ', this._name);
        return name;
    }

    _giveName(textCallback) {
        let name = this._name;
        let sigId;
        if (name === undefined) {
            sigId = this.connect('name-set', () => {
                textCallback(this.getName(), this._type);
                this.disconnect(sigId);
            });
        } else {
            textCallback(this.getName(), this._type);
        }
    }

});

var VirtualPort = class extends PopupMenu.PopupMenuItem {
    // Name: 'PulseVirtualPort',
    // Extends: PopupMenu.PopupMenuItem,

    _init(path, paconn, device) {
        this.parent('');
        this._paDBus = paconn;
        this._path = path;
        this._device = device;

        this._type = 'virtual';
        this._name = this._device._name;
        this.label.set_text(this.getName());
        this.emit('name-set');

        this.connect('activate', this._device._onPortSelect.bind(this._device));
    }

    getName() {
        let name = this._device._name;
        if (this._device._numPorts > 1)
            name = name.concat(': ', this._name);
        return name;
    }

    _giveName(textCallback) {
        let name = this._name;
        let sigId;
        if (name === undefined) {
            sigId = this.connect('name-set', () => {
                textCallback(this.getName(), this._type);
                this.disconnect(sigId);
            });
        } else {
            textCallback(this.getName(), this._type);
        }
    }

};


var Device = class {
    // Name: 'PulseDevice',

    constructor(path, paconn, base) {
        this._paDBus = paconn;
        this._path = path;
        this._ports = {};
        this._virtual = null;
        this._base = base;
        this._activePort = null;

        this._settings = Convenience.getSettings();
        this._key_PA_OVERDRIVE = Me.imports.prefs.KEY_PA_OVER;

        this._sigVol = this._sigMute = this._sigPort = 0;
        this._numPorts = 0;

        this._getDBusProperty('PropertyList').then(props => {
            let name;
            for (let i = 0; i < props.n_children(); i++) {
                let [index, value] = props.get_child_value(i).unpack();
                let key = index.get_string()[0];
                if (key == 'alsa.card_name' || key == 'device.description') {
                    name = ByteArray.toString(value.get_bytestring());
                    break;
                }
            }
            this._name = name;
        }).catch(logError);

        this._getDBusProperty('Ports').then(portPaths => {
            this._numPorts = portPaths.n_children();
            if (this._numPorts > 0) {
                this._virtual = null;
                for (let j = 0; j < this._numPorts; j++) {
                    let val = portPaths.get_child_value(j);
                    if (val != null) {
                        let portPath = val.get_string()[0];
                        let port = new Port(portPath, this._paDBus, this);
                        this._ports[portPath] = port;
                        this._base.menu.addMenuItem(port);

                        this._base._updateExpandBtnVisiblity();
                    }
                }
            } else if (this._base._type == 'Sink') {
                this._virtual = new VirtualPort(this._path, this._paDBus, this);
                this._base.menu.addMenuItem(this._virtual);
                this._base._updateExpandBtnVisiblity();
            }
        }).catch(logError);

        this._setOverdriveLevel();
        this._sigOverdrive = this._settings.connect(
            `changed::${this._key_PA_OVERDRIVE}`, this._setOverdriveLevel.bind(this));

        this._base.actor.connect('destroy', this._onDestroy.bind(this));
    }

    async _getDBusProperty(property) {
        return this._paDBus.call(
            null, this._path, 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.PulseAudio.Core1.Device', property]),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack());
    }

    async setActiveDevice() {
        this._sigVol = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1.Device', 'VolumeUpdated',
            this._path, null, Gio.DBusSignalFlags.NONE, this._onVolumeChanged.bind(this));

        const volP = this._getDBusProperty('Volume').then(this.setVolume.bind(this));

        this._sigMute = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1.Device', 'MuteUpdated',
            this._path, null, Gio.DBusSignalFlags.NONE, this._onVolumeChanged.bind(this));
        const muteP = this._getDBusProperty('Volume').then(this.setVolume.bind(this));


        if (this._numPorts > 0) {
            this._sigPort = this._paDBus.signal_subscribe(
                null, 'org.PulseAudio.Core1.Device', 'ActivePortUpdated',
                this._path, null, Gio.DBusSignalFlags.NONE, this._onPortChanged.bind(this));

            const port = await this._getDBusProperty('ActivePort').then(r => r.get_string()[0]);
            this.setActivePort(this._ports[port]);
        } else if (this._virtual != null) {
            this.setActivePort(this._virtual);
        } else {
            this.setDumbActivePort();
        }

        await Promise.all([volP, muteP]);

        this._base.emit('fallback-updated', this._path);
    }

    setDumbActivePort() {
        //Unset the currently active port
        if (this._activePort != null)
            this._activePort.setOrnament(PopupMenu.Ornament.NONE);

        this._activePort = null;

        this._base._nameLbl.set_text('');
        this._base._setMuteIcon('');
    }

    setActivePort(port) {
        //Unset the currently active port
        if (this._activePort != null)
            this._activePort.setOrnament(PopupMenu.Ornament.NONE);

        this._activePort = port;
        this._activePort.setOrnament(PopupMenu.Ornament.DOT);

        port._giveName((name, type) => {
            this._base._nameLbl.set_text(name);
            this._base._setMuteIcon(type);
        });
    }

    unsetActiveDevice() {
        if (this._activePort != null) {
            this._activePort.setOrnament(PopupMenu.Ornament.NONE);
        }
        this._activePort = null;

        this._paDBus.signal_unsubscribe(this._sigVol);
        this._paDBus.signal_unsubscribe(this._sigMute);
        if (this._numPorts > 0) {
            this._paDBus.signal_unsubscribe(this._sigPort);
        }

        this._sigVol = this._sigMute = this._sigPort = 0;
    }

    setVolume(volume) {
        this._base.emit('fallback-updated', this._path);
        if (typeof volume === 'boolean') {
            let val = GLib.Variant.new_boolean(volume);
            this._paDBus.call(null, this._path, 'org.freedesktop.DBus.Properties', 'Set',
                GLib.Variant.new('(ssv)', ['org.PulseAudio.Core1.Device', 'Mute', val]), null,
                Gio.DBusCallFlags.NONE, -1, null)
                .catch(logError);
        } else if (typeof volume === 'number') {
            if (volume > 1)
                volume = 1;
            let max = this._volVariant.get_child_value(0).get_uint32();
            for (let i = 1; i < this._volVariant.n_children(); i++) {
                let val = this._volVariant.get_child_value(i).get_uint32();
                if (val > max)
                    max = val;
            }

            let target = volume * this._getPAMaxPref();
            if (target != max) { //Otherwise no change
                let targets = new Array();
                for (let i = 0; i < this._volVariant.n_children(); i++) {
                    let newVal;
                    if (max == 0)
                        newVal = target;
                    else { //To maintain any balance the user has set.
                        let oldVal = this._volVariant.get_child_value(i).get_uint32();
                        newVal = (oldVal / max) * target;
                    }
                    newVal = Math.round(newVal);
                    targets[i] = GLib.Variant.new_uint32(newVal);
                }
                targets = GLib.Variant.new_array(null, targets);
                this._paDBus.call(null, this._path, 'org.freedesktop.DBus.Properties', 'Set',
                    GLib.Variant.new('(ssv)', ['org.PulseAudio.Core1.Device', 'Volume', targets]),
                    null, Gio.DBusCallFlags.NONE, -1, null)
                    .catch(logError);
                if (this._muteVal)
                    this.setVolume(false);
            }
        } else if (volume instanceof GLib.Variant) {
            let type = volume.get_type_string();
            if (type == 'au') {
                this._volVariant = volume;
                if (!this._muteVal) {
                    let maxVal = volume.get_child_value(0).get_uint32();
                    for (let i = 1; i < volume.n_children(); i++) {
                        let val = volume.get_child_value(i).get_uint32();
                        if (val > maxVal)
                            maxVal = val;
                    }

                    this._base._slider.value = maxVal / this._getPAMaxPref();
                }
            } else if (type == 'b') {
                this._muteVal = volume.get_boolean();
                if (this._muteVal)
                    this._base._slider.value = 0;
                else if (this._volVariant)
                    this.setVolume(this._volVariant);
            }

            this._base.emit('icon-changed', this._base._slider.value);
        }
    }

    _getPAMaxPref() {
        return (PA_MAX * this._pa_overdrive) / 100;
    }

    _setOverdriveLevel() {
        this._pa_overdrive = this._settings.get_int(this._key_PA_OVERDRIVE);
    }

    //Event handlers
    _onVolumeChanged(conn, sender, object, iface, signal, param) {
        if (signal == 'VolumeUpdated') {
            let vals = param.get_child_value(0);
            let startV = this._volVariant;

            let oldMax = startV.get_child_value(0).get_uint32();
            let newMax = vals.get_child_value(0).get_uint32();
            for (let i = 1; i < vals.n_children; i++) {
                let oVal = startV.get_child_value(i).get_uint32();
                let nVal = vals[i].get_uint32();

                if (oVal > oldMax)
                    oldMax = oVal;
                if (nVal > newMax)
                    newMax = nVal;
            }

            if (oldMax != newMax) { //Otherwise there is no change
                this._volVariant = vals;
                this._base._slider.value = newMax / this._getPAMaxPref();
            }
        } else if (signal == 'MuteUpdated') {
            this._muteVal = param.get_child_value(0).get_boolean();

            if (this._muteVal)
                this._base._slider.value = 0;
            else {
                let max = this._volVariant.get_child_value(0).get_uint32();
                for (let i = 1; i < this._volVariant.n_children(); i++) {
                    let val = this._volVariant.get_child_value(i).get_uint32();
                    if (max < val)
                        max = val;
                }
                this._base._slider.value = max / this._getPAMaxPref();
            }
        }
        this._base.emit('icon-changed', this._base._slider.value);
    }

    _onPortChanged(conn, sender, object, iface, signal, param) {
        let path = param.get_child_value(0).get_string()[0];
        this.setActivePort(this._ports[path]);
    }

    _onPortSelect(port) {
        if (this._numPorts > 0 && this._activePort != port) {
            let value = GLib.Variant.new_object_path(port._path);
            this._paDBus.call(null, this._path, 'org.freedesktop.DBus.Properties', 'Set',
                GLib.Variant.new('(ssv)', ['org.PulseAudio.Core1.Device', 'ActivePort', value]),
                null, Gio.DBusCallFlags.NONE, -1, null)
                .catch(logError);
        }

        if (this._base._activeDevice != this) {
            let value = GLib.Variant.new_object_path(this._path);
            this._paDBus.call(
                null, '/org/pulseaudio/core1', 'org.freedesktop.DBus.Properties', 'Set',
                GLib.Variant.new('(ssv)',
                    ['org.PulseAudio.Core1', `Fallback${this._base._type}`, value]), null,
                Gio.DBusCallFlags.NONE, -1, null)
                .catch(logError);
        }
    }

    _onDestroy() {
        this._settings.disconnect(this._sigOverdrive);
        if (this._sigVol != 0) {
            this._paDBus.signal_unsubscribe(this._sigVol);
            this._paDBus.signal_unsubscribe(this._sigMute);
            if (this._numPorts > 0) {
                this._paDBus.signal_unsubscribe(this._sigPort);
            }
        }
    }

    destroy() {
        for (let p in this._ports) {
            this._ports[p].destroy();
        }
        if (this._virtual != null) {
            this._virtual.destroy();
        }
    }
};
