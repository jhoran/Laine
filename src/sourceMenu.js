/* exported SourceMenu */

const {GLib, Gio, GObject} = imports.gi;

const Me = imports.misc.extensionUtils.getCurrentExtension();
const PortMenu = Me.imports.portMenu;

var SourceMenu = GObject.registerClass(
class SourceMenu extends PortMenu.PortMenu {

    _init(parent, paconn) {
        super._init(parent, paconn, 'Source');

        this._streams = new Array();

        this._initSources();

        this._sigNewStr = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1', 'NewRecordStream', '/org/pulseaudio/core1', null,
            Gio.DBusSignalFlags.NONE, this._onAddStream.bind(this));
        this._sigRemStr = this._paDBus.signal_subscribe(
            null, 'org.PulseAudio.Core1', 'RecordStreamRemoved', '/org/pulseaudio/core1', null,
            Gio.DBusSignalFlags.NONE, this._onRemoveStream.bind(this));

        this.connect('fallback-updated', this._onSetDefaultSource.bind(this));
        this.actor.connect('destroy', this._onSubDestroy.bind(this));
    }

    async _initSources() {
        const streamPromises = [];
        this._updateVisibility();
        await this._paDBus.call(
            null, '/org/pulseaudio/core1', 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.PulseAudio.Core1', 'RecordStreams']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => {
                const resp = r.get_child_value(0).unpack();
                for (let i = 0; i < resp.n_children(); i++)
                    streamPromises.push(this._addStream(resp.get_child_value(i).get_string()[0]));
            });

        await Promise.all(streamPromises);
    }

    async _addStream(path) {
        const rsMethod = await this._paDBus.call(
            null, path, 'org.freedesktop.DBus.Properties', 'Get',
            GLib.Variant.new('(ss)', ['org.PulseAudio.Core1.Stream', 'ResampleMethod']),
            GLib.VariantType.new('(v)'), Gio.DBusCallFlags.NONE, -1, null)
            .then(r => r.get_child_value(0).unpack().get_string()[0]);

        if (rsMethod == 'peaks')
            return;

        this._streams.push(path);
        this._updateVisibility();
    }

    _onAddStream(conn, sender, object, iface, signal, param) {
        let path = param.get_child_value(0).unpack();
        this._addStream(path).catch(logError);
    }

    _onRemoveStream(conn, sender, object, iface, signal, param) {
        let path = param.get_child_value(0).unpack();
        let index = this._streams.indexOf(path);
        if (index != -1) {
            this._streams.splice(index, 1);
            this._updateVisibility();
        }
    }


    _setMuteIcon(desc) {
        if (desc.endsWith('-mic'))
            this._icon.icon_name = 'audio-headset-symbolic';
        else
            this._icon.icon_name = 'audio-input-microphone-symbolic';
    }

    _isExpandBtnVisible() {
        let num = 0;
        for (let d in this._devices) {
            num += this._devices[d]._numPorts;
            if (num > 0) {
                return true;
            }
        }

        return false;
    }

    _isVisible() {
        return (this._streams.length > 0);
    }

    _onSetDefaultSource(src, source) {
        for (let s in this._streams) {
            this._paDBus.call(null, this._streams[s], 'org.PulseAudio.Core1.Stream', 'Move',
                GLib.Variant.new('(o)', [source]), null, Gio.DBusCallFlags.NONE, -1, null)
                .catch(logError);
        }
    }

    _onSubDestroy() {
        this._paDBus.signal_unsubscribe(this._sigNewStr);
        this._paDBus.signal_unsubscribe(this._sigRemStr);
    }
});
