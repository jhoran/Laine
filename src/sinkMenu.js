/* exported SinkMenu */
const GObject = imports.gi.GObject;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const PortMenu = Me.imports.portMenu;

var SinkMenu = GObject.registerClass(
class SinkMenu extends PortMenu.PortMenu {
    // Name: 'SinkMenu',
    // Extends: PortMenu.PortMenu,

    _init(parent, paconn) {
        super._init(parent, paconn, 'Sink');
    }

    _setMuteIcon(desc) {
        if (desc.endsWith('headphones'))
            this._icon.icon_name = 'audio-headphones-symbolic';
        else if (desc.startsWith('hdmi') || desc.startsWith('iec958'))
            this._icon.icon_name = 'audio-card-symbolic';
        else if (desc.endsWith('virtual'))
            this._icon.icon_name = 'audio-x-generic-symbolic';
        else
            this._icon.icon_name = 'audio-speakers-symbolic';
    }

    _isExpandBtnVisible() {
        let num = 0;
        for (let d in this._devices) {
            num += this._devices[d]._numPorts;
            if (num > 0) {
                return true;
            }
        }

        return false;
    }

    _isVisible() {
        return true;
    }
});
