/*exported unpackArrayElements*/
function unpackArrayElements(reply) {
    const elements = reply.get_child_value(0).unpack();
    const result = [];
    for (let i = 0; i < elements.n_children(); i++)
        result.push(elements.get_child_value(i).get_string()[0]);
    return result;
}